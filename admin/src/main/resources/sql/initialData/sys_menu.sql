/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云labFrame
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : aliyun.labmem.xyz:33069
 Source Schema         : labFrame

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 07/12/2020 11:53:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父id',
  `state` enum('ON','OFF','DEL') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '数据状态，ON为数据启用，OFF为数据停用但仍在前端显示，DEL为数据对用户来说已经删除，只在数据库中做保留',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '数据更新时间',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单标题',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `type` int(2) NOT NULL COMMENT '菜单类型 0、父菜单1、菜单按钮',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单地址',
  `sort` int(3) NOT NULL DEFAULT 99 COMMENT '排序',
  `open_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开类型',
  `role_list` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限列',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, NULL, 'ON', '2020-05-08 14:03:27', '2020-06-23 17:30:31', '系统管理', 'layui-icon layui-icon-home', 0, '', 2, '', '');
INSERT INTO `sys_menu` VALUES (2, 1, 'ON', '2020-05-19 15:40:54', '2020-06-09 13:01:19', '菜单管理', NULL, 1, '/admin/menu/index', 1, '_iframe', '');
INSERT INTO `sys_menu` VALUES (4, 10, 'ON', '2020-05-19 15:59:21', '2020-06-17 14:30:23', '权限角色', NULL, 1, '/admin/authority/index', 1, '_iframe', '');
INSERT INTO `sys_menu` VALUES (6, 10, 'ON', '2020-06-09 14:50:15', '2020-06-17 14:30:28', '权限管理', NULL, 1, '/admin/authorityApi/index', 2, '_iframe', '');
INSERT INTO `sys_menu` VALUES (9, 10, 'ON', '2020-06-09 17:14:33', '2020-06-17 14:30:32', '用户管理', NULL, 1, '/admin/user/index', 3, '_iframe', '');
INSERT INTO `sys_menu` VALUES (10, NULL, 'ON', '2020-06-17 14:29:28', '2020-06-18 09:51:12', '权限管理', 'layui-icon layui-icon-friends', 0, '', 3, NULL, '[\"ADMIN\"]');
INSERT INTO `sys_menu` VALUES (11, 1, 'ON', '2020-06-17 14:31:58', '2020-06-17 14:31:58', '文件管理', NULL, 1, '', 2, '_iframe', '');
INSERT INTO `sys_menu` VALUES (12, 1, 'ON', '2020-06-17 14:32:18', '2020-06-24 09:34:40', '日志管理', 'layui-icon layui-icon-form', 0, '', 3, '_iframe', '');
INSERT INTO `sys_menu` VALUES (13, NULL, 'OFF', '2020-06-18 09:51:03', '2020-06-23 17:34:30', '首页', NULL, 1, 'index', 1, '_iframe', '');
INSERT INTO `sys_menu` VALUES (14, 12, 'ON', '2020-06-24 11:53:15', '2020-06-24 11:53:15', '登录日志', NULL, 1, '/admin/log/login', 1, '_iframe', '');
INSERT INTO `sys_menu` VALUES (15, NULL, 'ON', '2020-07-07 11:53:59', '2020-07-07 11:53:59', '项目管理', 'layui-icon layui-icon-tabs', 0, '', 3, NULL, '');
INSERT INTO `sys_menu` VALUES (16, 15, 'ON', '2020-07-07 11:54:36', '2020-07-07 11:54:36', 'swagger', NULL, 1, '/doc.html', 1, '_iframe', '');

SET FOREIGN_KEY_CHECKS = 1;
