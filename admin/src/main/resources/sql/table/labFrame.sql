CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '账号',
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '昵称',
  `head_img` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像文件key',
  `role` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限',
  `admin` int(2) NOT NULL DEFAULT '0' COMMENT '是否是后台管理账号',
  `state` enum('ON','OFF','DEL') CHARACTER SET utf8mb4 NOT NULL DEFAULT 'ON' COMMENT '数据状态，ON为数据启用，OFF为数据停用但仍在前端显示，DEL为数据对用户来说已经删除，只在数据库中做保留',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `sys_role_valid` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `comment` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '接口注释',
  `class_name` varchar(500) COLLATE utf8mb4_bin NOT NULL COMMENT '接口类名',
  `method_name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '接口方法名',
  `role_list` text COLLATE utf8mb4_bin NOT NULL COMMENT '接口对应的权限JSONString',
  `is_api` int(2) NOT NULL DEFAULT '0' COMMENT '是否是API接口',
  `state` enum('ON','OFF','DEL') CHARACTER SET utf8mb4 NOT NULL DEFAULT 'ON' COMMENT '数据状态，ON为数据启用，OFF为数据停用但仍在前端显示，DEL为数据对用户来说已经删除，只在数据库中做保留',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '权限名称',
  `role_enum` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '权限枚举名称',
  `remarks` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `role_default` int(2) NOT NULL DEFAULT '0' COMMENT '是否是默认权限',
  `state` enum('ON','OFF','DEL') CHARACTER SET utf8mb4 NOT NULL DEFAULT 'ON' COMMENT '数据状态，ON为数据启用，OFF为数据停用但仍在前端显示，DEL为数据对用户来说已经删除，只在数据库中做保留',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `role_enum` (`role_enum`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `state` enum('ON','OFF','DEL') NOT NULL DEFAULT 'ON' COMMENT '数据状态，ON为数据启用，OFF为数据停用但仍在前端显示，DEL为数据对用户来说已经删除，只在数据库中做保留',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
  `original_name` varchar(255) NOT NULL COMMENT '文件原名',
  `file_path` varchar(500) NOT NULL COMMENT '文件保存路径',
  `file_key` varchar(200) NOT NULL COMMENT '文件下载key',
  `file_size` varchar(50) DEFAULT NULL COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='上传文件表';