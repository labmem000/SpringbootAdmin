if (window != top)
    top.location.href = location.href;
var pearOper, tool;
layui.use(['pearOper', 'jquery', 'tool', 'request'], function () {
    pearOper = layui.pearOper;
    tool = layui.tool;
    var $ = layui.jquery;
    var request = layui.request;
    var remember = tool.cookies.get("labmem_remember");
    if (remember != null) {
        tool.JsonToHtmlValByName(JSON.parse(remember), $("#loginForm"))
    }
    $("body").on("click", "#login", function () {
        var data = tool.serializeObject($("#loginForm"));
        request.post({
            needLogin: false,
            url: 'auth/adminLogin',
            data: data
        }, function (res) {
            if (res.code === 0) {
                tool.cookies.set("labmem_token", res.data);
                if (data.remember === 'on')
                    tool.cookies.set("labmem_remember", JSON.stringify(data));
                else
                    tool.cookies.set("labmem_remember", null);
                location.href = "/"
            }
        });
    })
});
