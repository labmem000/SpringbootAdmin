var $, request;
layui.use(['pearAdmin', 'jquery', 'pearTab', 'request', 'tool', 'pearOper', 'layer'], function () {
    var pearAdmin = layui.pearAdmin;
    $ = layui.jquery;
    var pearTab = layui.pearTab;
    var pearOper = layui.pearOper;
    request = layui.request;
    var tool = layui.tool;

    $("body").on("click", ".pearson", function () {
        pearTab.addTabOnlyByElem("content", {
            id: 111,
            title: "个人信息",
            url: "/admin/user/person",
            close: true
        })
    })

    /**
     * 注销
     */
    $("#Logout").click(function () {
        pearOper.confirm({
            title: "提示",
            message: "您确定要注销吗",
            success: function () {
                request.post({
                    url: 'auth/logout'
                }, function (res) {
                    tool.cookies.del("labmem_token")
                    location.href = "/"
                });
            }
        });
    })

    /**
     * 框架初始化配置
     * */
    pearAdmin.render({
        keepLoad: 1000, // 主 页 加 载 过 度 时 长 可为 false
        muiltTab: true, // 是 否 开 启 多 标 签 页 true 开启 false 关闭
        control: false, // 是 否 开 启 多 系 统 菜 单 true 开启 false 关闭
        theme: "light", // 默 认 主 题 样 式 dark 默认主题 light 亮主题
        index: 'index', // 默 认 加 载 主 页
        data: '/admin/menu/api/tree', // 菜 单 数 据 加 载 地 址
        select: '0',
        notice: false,
        auth: false,
        done: function () {
            /** 框架初始化 */
            console.log("%c┏┛ ┻━━━━━┛ ┻┓\n" +
                "┃　 　 ━　　  ┃\n" +
                "┃　┳┛　  ┗┳   ┃\n" +
                "┃　　　 -　　  ┃\n" +
                "┗━┓　　　 ┏━━━┛\n" +
                "   ┃　　　 ┗━━━━━━━━┓\n" +
                "   ┗┓ ┓  ┏━━┳ ┓  ┏━┛\n" +
                "    ┗━┻━┛   ┗━┻━┛ labmem-lty", "color:#41C7FF");
        }
    });

    // /**
    //  * 消息组件初始化
    //  * */
    // pearNotice.render({
    //     elem: 'headerNotice',
    //     url: 'admin/data/notice.json',
    //     height: '200px',
    //     click: function (id, title) {
    //         layer.msg("当前监听消息" + id + "消息标题:" + title);
    //     }
    // });


});