layui.define(['jquery', 'tool', 'pearOper'], function (exports) {
    var $ = layui.jquery
        , tool = layui.tool
        , pearOper = layui.pearOper;
    var request = {
        get: function (options, _callback) {
            options.type = 'get';
            request.ajax(options, _callback);
        }
        , post: function (options, _callback) {
            options.type = 'post';
            if (options.data !== false)
                request.ajax(options, _callback);
        }
        , put: function (options, _callback) {
            options.type = 'put';
            request.ajax(options, _callback);
        }, delete: function (options, _callback) {
            options.type = 'delete';
            request.ajax(options, _callback);
        }, upload: function (options, _callback) {
            if (options.load === undefined || options.load === true)
                pearOper.Load(3, "请稍后..");
            $.ajax({
                url: options.url,
                type: 'post',
                data: options.data,
                processData: false,  // 告诉jQuery不要去处理发送的数据
                contentType: false, // 告诉jQuery不要去设置Content-Type请求头
                beforeSend: function (XMLHttpRequest) {
                    if (options.needLogin === undefined || options.needLogin === true) {
                        var token = tool.cookies.get('zcah_token');
                        var modular = tool.cookies.get('zcah_modular');
                        XMLHttpRequest.setRequestHeader("zcah_token", token);
                        XMLHttpRequest.setRequestHeader("zcah_modular", modular);
                    }
                },
                success: function (res) {
                    if (options.load === undefined || options.load === true)
                        pearOper.loadRemove(500);
                    if (options.msg === undefined || options.msg === true)
                        if (res.code === 0)
                            tool.pear.success(res.message === undefined ? "成功" : res.message);
                        else
                            tool.pear.fail(res.message === undefined ? "失败" : res.message);
                    if (typeof _callback == 'function')
                        _callback(res);
                },
                error: function (err) {
                    pearOper.loadRemove(500);
                    tool.pear.error("服务器错误")
                }
            })
        }
        , ajax: function (options, _callback) {
            if (options.load === undefined || options.load === true)
                pearOper.Load(3, "请稍后..");
            $.ajax({
                url: options.url
                , type: options.type
                , data: options.type === 'post' ? JSON.stringify(options.data === undefined ? {} : options.data) : ''
                , dataTypeString: 'json'
                , contentType: 'application/json;charset=utf-8'
                , beforeSend: function (XMLHttpRequest) {
                    if (options.needLogin === undefined || options.needLogin === true) {
                        var token = tool.cookies.get('zcah_token');
                        var modular = tool.cookies.get('zcah_modular');
                        XMLHttpRequest.setRequestHeader("zcah_token", token);
                        XMLHttpRequest.setRequestHeader("zcah_modular", modular);
                    }
                }
                , success: function (res) {
                    if (options.load === undefined || options.load === true)
                        pearOper.loadRemove(500);
                    if (options.msg === undefined || options.msg === true)
                        if (res.code === 0)
                            tool.pear.success(res.message === undefined ? "成功" : res.message);
                        else if (res.code === -2)
                            $.each(res.data, function (index, item) {
                                tool.pear.fail(res.message + "；'" + item.field + "' " + item.message);
                            })
                        else if (res.code === -3) {//登录失效
                            tool.pear.error(res.message === undefined ? "失败" : res.message);
                            setTimeout(function () {
                                location.href = "/";
                            }, 2000)
                        } else
                            tool.pear.fail(res.message === undefined ? "失败" : res.message);
                    if (typeof _callback == 'function')
                        _callback(res);
                }
                , error: function (err) {
                    pearOper.loadRemove(500);
                    tool.pear.error("服务器错误")
                }
            })
        }
    };
    exports('request', request)
});