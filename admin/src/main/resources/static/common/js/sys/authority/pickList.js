var $, tool, pickList = {}, AllData = [], request;

function Init(roleList, Headx) {
    layui.use(['table', 'form', 'jquery', 'treetable', 'request', 'tool'], function () {
        let table = layui.table;
        $ = layui.jquery;
        tool = layui.tool;
        request = layui.request;
        let MODULE_PATH = "/admin/authority/api";
        if (Headx)
            $("#headx").css('display', '');
        table.render({
            elem: '#power-table',
            url: MODULE_PATH + '/list',
            skin: 'line',
            height: tool.getLayuiTableHeight(),
            method: 'post',
            contentType: "application/json",
            page: false,
            parseData: function (res) { //res 即为原始返回的数据
                if (roleList !== '' && roleList != null) {
                    var oldList = roleList.split(",");
                    $.each(res.data, function (index, item) {
                        if (oldList.indexOf(item.id) >= 0) {
                            //如果set集合中有的话，给rows添加check属性选中
                            res.data[index]["LAY_CHECKED"] = true;
                            pickList[item.id] = item.roleName;
                        }
                    })
                }
                return {
                    "code": res.code, //解析接口状态
                    "count": res.count, //解析数据长度
                    "data": res.data //解析数据列表
                };
            },
            done: function (res, curr, count) {
                AllData = res.data;
            },
            cols: [
                [
                    {type: 'checkbox'},
                    {field: 'roleName', title: '权限名称'},
                    {field: 'roleEnum', title: '权限枚举'}
                ]
            ]
        });

        table.on('checkbox(power-table)', function (obj) {
            if (obj.checked) {
                if (obj.type === 'all') {//全选
                    $.each(AllData, function (index, item) {
                        pickList[item.id] = item.roleName;
                    })
                } else {//选中
                    pickList[obj.data.id] = obj.data.roleName;
                }
            } else {
                if (obj.type === 'all') {//全取消
                    pickList = {};
                } else {//取消
                    delete pickList[obj.data.id];
                }
            }
        });
    });
}

function getHeadx() {
    return tool.serializeObject($("#headx"));
}