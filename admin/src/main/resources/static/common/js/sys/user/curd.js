var curdObj, tool, $;

function init(data) {
    curdObj = data;
    layui.use(['form', 'jquery', 'request', 'tool'], function () {
        let table = layui.table;
        let form = layui.form;
        $ = layui.jquery;
        let request = layui.request;
        tool = layui.tool;

        //初始化权限列表
        request.post({
            load: false,
            msg: false,
            url: '/admin/authority/api/list',
        }, function (res) {
            var data = [];
            $.each(res.data, function (index, item) {
                data.push({
                    name: item.roleName,
                    value: item.roleEnum
                });
            });
            let roleSelect = xmSelect.render({
                el: '#roleSelect',
                radio: true,
                tips: '请选择权限角色',
                filterable: true,
                clickClose: true,
                data: data,
                model: {
                    label: {
                        type: 'text',
                        text: {
                            //左边拼接的字符
                            left: '',
                            //右边拼接的字符
                            right: '',
                            //中间的分隔符
                            separator: ', ',
                        },
                    }
                },
                height: '160px',
                name: 'role'
            })

            if (curd === 'edit') {
                tool.JsonToHtmlValByName(curdObj, $("#curdForm"));
                $("input[name='userName']").attr("readonly", "readonly");
                $("#ts").css("display", "none");
                roleSelect.setValue([curdObj.role]);
                if (curdObj.admin == '0')
                    $("#admin").removeAttr("checked");
            }
            form.render();
        });


        //监听switch
        form.on('switch(adminEnable)', function (data) {
            var enable = this.checked ? '1' : '0';
            $("input[name='admin']").val(enable);
        });

    });
}

/**
 * 获取表单内容
 */
function formData() {
    var data = tool.serializeObject($("#curdForm"));
    if(tool.isBlank(data.userName)){
        tool.pear.fail("请填写用户账号")
        return false;
    }
    if(tool.isBlank(data.role)){
        tool.pear.fail("请选择权限角色")
        return false;
    }
    if(tool.isBlank(data.nickName)){
        tool.pear.fail("请填写用户昵称")
        return false;
    }
    if (curd === 'edit')
        data.id = curdObj.id;
    return data;
}