let roleSelect;
layui.use(['table', 'form', 'tree', 'jquery', 'request', 'tool', 'pearOper'], function () {
    let table = layui.table;
    let form = layui.form;
    let $ = layui.jquery;
    let request = layui.request;
    let tool = layui.tool;
    var tree = layui.tree;
    var pearOper = layui.pearOper;

    let MODULE_PATH = "/admin/user/api";

    //初始化权限列表
    request.post({
        load: false,
        msg: false,
        url: '/admin/authority/api/list',
    }, function (res) {
        var data = [];
        $.each(res.data, function (index, item) {
            data.push({
                name: item.roleName,
                value: item.roleEnum
            });
        });
        roleSelect = xmSelect.render({
            el: '#roleSelect',
            radio: tree,
            tips: '请选择权限角色',
            filterable: true,
            data: data,
            clickClose: true,
            model: {
                label: {
                    type: 'text',
                    text: {
                        //左边拼接的字符
                        left: '',
                        //右边拼接的字符
                        right: '',
                        //中间的分隔符
                        separator: ', ',
                    },
                }
            },
            name: 'role'
        })
        Init();
    });

    function Init() {
        var searchForm = $("#searchForm");
        var where = tool.serializeObject(searchForm);
        table.render({
            elem: '#power-table',
            toolbar: '#power-toolbar',
            url: MODULE_PATH + '/list',
            skin: 'line',
            height: tool.getLayuiTableHeight(searchForm.height() * 2 + 60),
            method: 'post',
            contentType: "application/json",
            where: where,
            page: true,
            done: function (res, curr, count) {
                //监听switch
                form.on('switch(switchEnable)', function (data) {
                    var id = layui.$(this).attr("switchId");
                    var enable = this.checked ? '启用' : '停用';
                    request.post({
                        url: MODULE_PATH + '/addOrUpdate',
                        data: {
                            id: id,
                            state: enable
                        }
                    }, function (res) {
                        Init();
                    });
                });
            },
            cols: [
                [
                    {title: '#', templet: '#index', fixed: 'left', width: 50, unresize: true},
                    {field: 'nickName', title: '用户昵称'},
                    {field: 'userName', title: '用户账号'},
                    {field: 'email', title: '用户邮箱'},
                    {field: 'roleName', title: '用户角色'},
                    {
                        field: 'admin', title: '是否是管理员', templet: function (d) {
                            if (d.isAdmin == '1')
                                return '是';
                            else if (d.isAdmin == '2')
                                return '超级管理员'
                            else
                                return '否';
                        }
                    },
                    {
                        field: 'state', title: '状态', templet: function (d) {
                            var switchBtn = '<input type="checkbox" lay-skin="switch" switchId=' + d.id + ' lay-filter="switchEnable" lay-text="启用|停用" ';
                            if (d.state == '启用')
                                switchBtn += "checked";
                            return switchBtn + ' >';
                        }
                    },
                    {title: '操作', templet: '#power-bar', width: 300, align: 'center'}
                ]
            ]
        });
    }

    table.on('tool(power-table)', function (obj) {
        switch (obj.event) {
            case "remove":
                if (obj.data["isAdmin"] !== '2')
                    remove(obj.data);
                else
                    tool.pear.fail("不能删除超级管理员");
                break;
            case "edit":
                if (obj.data["isAdmin"] !== '2')
                    edit(obj.data);
                else
                    tool.pear.fail("不能修改超级管理员");
                break;
            case "resetPWD":
                resetPWD(obj.data);
                break;
        }
    });


    table.on('toolbar(power-table)', function (obj) {
        switch (obj.event) {
            case "add":
                add();
                break;
            case "search":
                Init();
                break;
        }
    });


    function add() {
        layer.open({
            type: 2,
            title: '创建用户',
            shade: 0.1,
            area: ['60%', '50%'],
            btn: ['提交'],
            content: '/admin/user/curd?curd=add',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init();
            },
            end: function (layero, index) {
                Init();
            }
        });
    }

    function edit(data) {
        layer.open({
            type: 2,
            title: '修改用户',
            shade: 0.1,
            area: ['60%', '50%'],
            btn: ['提交'],
            content: '/admin/user/curd?curd=edit',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init(data);
            },
            end: function (layero, index) {
                Init();
            }
        });
    }

    function remove(data) {
        pearOper.confirm({
            title: "提示",
            message: "确定要删除该用户吗",
            success: function () {
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: {
                        id: data.id,
                        state: '删除'
                    }
                }, function (data) {
                    Init();
                });
            }
        });
    }

    function resetPWD(data) {
        pearOper.confirm({
            title: "提示",
            message: "确定要重置该用户的密码吗",
            success: function () {
                request.post({
                    url: MODULE_PATH + '/resetPwd',
                    data: {
                        id: data.id
                    }
                }, function (data) {
                    Init();
                });
            }
        });
    }

});

function reset() {
    document.getElementById('searchForm').reset();
    roleSelect.setValue([]);
}