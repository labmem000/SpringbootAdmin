
layui.use(['table', 'form', 'jquery', 'treetable', 'request', 'tool'], function () {
    let table = layui.table;
    let form = layui.form;
    let $ = layui.jquery;
    let request = layui.request;
    let tool = layui.tool;
    let treetable = layui.treetable;

    let MODULE_PATH = "/admin/menu/api";
    request.post({
        url: MODULE_PATH + '/list',
        data: tool.serializeObject($("searchForm")),
        msg: false,
        load: false
    }, function (res) {
        treetableInit(res.data);
    });

    function treetableInit(data) {
        treetable.render({
            treeColIndex: 0,
            treeSpid: '',
            treeIdName: 'id',
            treePidName: 'pid',
            skin: 'line',
            treeDefaultClose: false,
            elem: '#power-table',
            data: data,
            height: 400,
            page: false,
            cols: [
                [
                    {field: 'title', minWidth: 200, title: '菜单名称'},
                    {title: '操作', templet: '#power-bar', width: 150, align: 'center'}
                ]
            ]
        });
    }

    table.on('tool(power-table)', function (obj) {
        switch (obj.event) {
            case "pick":
                pick(obj.data);
                break;
        }
    });

    function pick(data) {
        parent.window.pickData = data;
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);//关闭当前页
    }

});