layui.use(['table', 'form', 'jquery', 'treetable', 'request', 'tool', 'pearOper'], function () {
    let table = layui.table;
    let form = layui.form;
    let $ = layui.jquery;
    let request = layui.request;
    let tool = layui.tool;
    let treetable = layui.treetable;
    var pearOper = layui.pearOper;

    let listData;
    let MODULE_PATH = "/admin/menu/api";
    postData();

    function postData() {
        request.post({
            url: MODULE_PATH + '/list',
            msg: false,
            load: false
        }, function (res) {
            treetableInit(res.data);
        });
    }

    function treetableInit(data) {
        listData = data;
        treetable.render({
            treeColIndex: 0,
            treeSpid: '',
            treeIdName: 'id',
            treePidName: 'pid',
            skin: 'line',
            treeDefaultClose: true,
            toolbar: '#power-toolbar',
            elem: '#power-table',
            data: data,
            height: tool.getLayuiTableHeight(),
            page: false,
            done: function (res, curr, count) {
                //监听switch
                form.on('switch(switchEnable)', function (data) {
                    var id = layui.$(this).attr("switchId");
                    var enable = this.checked ? '启用' : '停用';
                    request.post({
                        url: MODULE_PATH + '/addOrUpdate',
                        data: {
                            id: id,
                            state: enable
                        }
                    });
                });
            },
            cols: [
                [
                    {field: 'title', minWidth: 200, title: '菜单名称'},
                    {field: 'icon', title: '图标', templet: '#icon'},
                    {field: 'roleString', title: '权限类型'},
                    {
                        field: 'state', title: '是否可用', templet: function (d) {
                            var switchBtn = '<input type="checkbox" lay-skin="switch" switchId=' + d.id + ' lay-filter="switchEnable" lay-text="启用|禁用" ';
                            if (d.state == 'ON')
                                switchBtn += "checked";
                            return switchBtn + ' >';
                        }
                    },
                    {field: 'sort', title: '排序'},
                    {title: '操作', templet: '#power-bar', width: 200, align: 'center'}
                ]
            ]
        });
    }

    table.on('tool(power-table)', function (obj) {
        console.log(obj)
        switch (obj.event) {
            case "remove":
                remove(obj.data);
                break;
            case "edit":
                edit(obj.data);
                break;
        }
    });


    table.on('toolbar(power-table)', function (obj) {
        switch (obj.event) {
            case "add":
                add();
                break;
            case "query":
                postData();
                break;
        }
    });


    function add() {
        layer.open({
            type: 2,
            title: '新增菜单',
            shade: 0.1,
            area: ['80%', '80%'],
            btn: ['提交'],
            content: '/admin/menu/curd?curd=add',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init();
            },
            end: function (layero, index) {
                postData();
            }
        });
    }

    function edit(data) {
        if (data.pid != null)
            $.each(listData, function (index, item) {
                if (data.pid === item.id)
                    data.pTitle = item.title
            });
        layer.open({
            type: 2,
            title: '修改菜单',
            shade: 0.1,
            area: ['80%', '80%'],
            btn: ['提交'],
            content: '/admin/menu/curd?curd=edit',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init(data);
            },
            end: function (layero, index) {
                postData();
            }
        });
    }

    function remove(data) {
        pearOper.confirm({
            title: "提示",
            message: "确定要删除该菜单吗",
            success: function () {
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: {
                        id: data.id,
                        state: '删除'
                    }
                }, function (data) {
                    postData();
                });
            }
        });
    }

});