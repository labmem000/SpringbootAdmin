let roleSelect;
layui.use(['table', 'form', 'laydate', 'jquery', 'request', 'tool', 'pearOper'], function () {
    let table = layui.table;
    let form = layui.form;
    let $ = layui.jquery;
    let request = layui.request;
    let tool = layui.tool;
    var laydate = layui.laydate;
    var pearOper = layui.pearOper;

    laydate.render({
        elem: '#loginDate' //指定元素
        , type: 'date'
        , format: 'yyyy-MM-dd'
    });

    let MODULE_PATH = "/admin/loginLog/api";
    Init();

    function Init() {
        var searchForm = $("#searchForm");
        var where = tool.serializeObject(searchForm);
        table.render({
            elem: '#power-table',
            toolbar: '#power-toolbar',
            url: MODULE_PATH + '/list',
            skin: 'line',
            height: tool.getLayuiTableHeight(searchForm.height() * 2 + 60),
            method: 'post',
            contentType: "application/json",
            where: where,
            page: true,
            done: function (res, curr, count) {
            },
            cols: [
                [
                    {title: '#', templet: '#index', fixed: 'left', width: 50, unresize: true},
                    {field: 'userName', title: '账号'},
                    {field: 'ip', title: 'ip地址'},
                    {field: 'browser', title: '浏览器信息'},
                    {field: 'message', title: '操作信息'},
                    {field: 'loginDate', title: '登录时间'}
                ]
            ]
        });
    }

    table.on('toolbar(power-table)', function (obj) {
        switch (obj.event) {
            case "search":
                Init();
                break;
        }
    });


});

function reset() {
    document.getElementById('searchForm').reset();
}