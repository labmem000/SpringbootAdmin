var curdObj, tool, $, roleList;

function init(data) {
    curdObj = data;
    let MODULE_PATH = "/admin/menu/api";
    layui.use(['form', 'jquery', 'request', 'tool'], function () {
        let table = layui.table;
        let form = layui.form;
        $ = layui.jquery;
        let request = layui.request;
        tool = layui.tool;
        form.render();
        form.on('radio(type)', function (data) {
            if (data.value == '0') {
                $("#openType").css("display", "none");
                $("#href").css("display", "none");
                $("#icon").css("display", "");
            } else {
                $("#openType").css("display", "");
                $("#href").css("display", "");
                $("#icon").css("display", "none");
            }
        });

        $(".site-doc-icon li").click(function () {
            var Iclass = $(this).find("i").attr('class');
            $("#icon [name='icon']").val(Iclass);
        });
        $("#pmenuName").click(function () {
            parent.layer.open({
                type: 2,
                title: '选择父菜单',
                shade: 0.1,
                area: ['450px', '600px'],
                content: '/admin/menu/pickList',
                end: function (layero, index) {
                    var pickData = parent.window.pickData;
                    if (pickData != null) {
                        $("#pmenuName").val(pickData.title);
                        $("#pmenu [name='pid']").val(pickData.id);
                        parent.window.pickData = undefined;
                    }
                }
            });
        });
        $("#roleListName").click(function () {
            parent.layer.open({
                type: 2,
                title: '选择菜单权限',
                shade: 0.1,
                area: ['450px', '600px'],
                content: '/admin/authority/pickList',
                btn: ['确定'],
                yes: function (index, layero) {
                    var iframeWin = parent.window[layero.find('iframe')[0]['name']];
                    var pickList = iframeWin.pickList;
                    var name = '';
                    roleList = '';
                    for (const key in pickList) {
                        name += pickList[key] + ',';
                        roleList += key + ',';
                    }
                    $("#roleListName").val(name);
                    parent.layer.close(index);
                },
                success: function (layero, index) {
                    var iframeWin = parent.window[layero.find('iframe')[0]['name']];
                    iframeWin.Init(roleList);
                },
            });
        });


        if (curd === 'edit') {
            var type = data.type;
            roleList = data.roleList;
            $("#type" + type).next().click();
            $("#pmenuName").val(data.pTitle);
            $("input[name='title']").val(data.title);
            $("input[name='pid']").val(data.pid);
            $("input[name='sort']").val(data.sort);
            $("#roleListName").val(data.roleString);
            if (type == '0')
                $("input[name='icon']").val(data.icon);
            else {
                $("#openType" + data.openType).next().click();
                $("input[name='href']").val(data.href);
            }
        }
    });
}

/**
 * 获取表单内容
 */
function formData() {
    var data = tool.serializeObject($("#curdForm"));
    if (tool.isBlank(data.title)) {
        tool.pear.fail("请填写菜单名称")
        return false;
    }
    if (curd === 'edit')
        data.id = curdObj.id;
    if (data.pid === '')
        data.pid = null;
    if (data.type == '0') {
        data.openType = null;
        data.href = null;
    } else
        data.icon = null;
    if (roleList !== '' && roleList != null) {
        var list = roleList.split(",");
        list.pop();
        data.roleList = JSON.stringify(list);
    } else
        data.roleList = null;
    return data;
}