layui.use(['table', 'form', 'jquery', 'request', 'tool', 'pearOper'], function () {
    let table = layui.table;
    let form = layui.form;
    let $ = layui.jquery;
    let request = layui.request;
    let tool = layui.tool;
    var pearOper = layui.pearOper;

    let MODULE_PATH = "/admin/authority/api";
    Init();

    function Init() {
        var searchForm = $("#searchForm");
        table.render({
            elem: '#power-table',
            toolbar: '#power-toolbar',
            url: MODULE_PATH + '/pageList',
            skin: 'line',
            height: tool.getLayuiTableHeight(searchForm.height() * 2 + 60),
            method: 'post',
            contentType: "application/json",
            where: tool.serializeObject(searchForm),
            page: true,
            done: function (res, curr, count) {
                //监听switch
                form.on('switch(switchEnable)', function (data) {
                    var id = layui.$(this).attr("switchId");
                    var enable = this.checked ? '启用' : '停用';
                    request.post({
                        url: MODULE_PATH + '/addOrUpdate',
                        data: {
                            id: id,
                            state: enable
                        }
                    }, function (res) {
                        Init();
                    });
                });
                //监听switch
                form.on('switch(switchDef)', function (data) {
                    var id = layui.$(this).attr("switchId");
                    var enable = this.checked ? '1' : '0';
                    request.post({
                        url: MODULE_PATH + '/addOrUpdate',
                        data: {
                            id: id,
                            roleDefault: enable
                        }
                    }, function (res) {
                        Init();
                    });
                });
            },
            cols: [
                [
                    {title: '#', templet: '#index', fixed: 'left', width: 50, unresize: true},
                    {field: 'roleName', title: '权限名称'},
                    {field: 'roleEnum', title: '权限枚举'},
                    {
                        title: '设为默认接口权限', templet: function (d) {

                            var switchBtn = '<input type="checkbox" lay-skin="switch" switchId=' + d.id + ' lay-filter="switchDef" lay-text="是|否" ';
                            if (d.roleDefault == '1')
                                switchBtn += "checked";
                            return switchBtn + ' >';
                        }
                    },
                    {
                        field: 'state', title: '状态', templet: function (d) {
                            var switchBtn = '<input type="checkbox" lay-skin="switch" switchId=' + d.id + ' lay-filter="switchEnable" lay-text="启用|停用" ';
                            if (d.state == '启用')
                                switchBtn += "checked";
                            return switchBtn + ' >';
                        }
                    },
                    {title: '操作', templet: '#power-bar', width: 200, align: 'center'}
                ]
            ]
        });
    }

    table.on('tool(power-table)', function (obj) {
        switch (obj.event) {
            case "remove":
                remove(obj.data);
                break;
            case "edit":
                edit(obj.data);
                break;
        }
    });


    table.on('toolbar(power-table)', function (obj) {
        switch (obj.event) {
            case "add":
                add();
                break;
            case "search":
                Init();
                break;
        }
    });


    function add() {
        layer.open({
            type: 2,
            title: '新增权限',
            shade: 0.1,
            area: ['80%', '60%'],
            btn: ['提交'],
            content: '/admin/authority/curd?curd=add',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init();
            },
            end: function (layero, index) {
                Init();
            }
        });
    }

    function edit(data) {
        layer.open({
            type: 2,
            title: '修改权限',
            shade: 0.1,
            area: ['80%', '60%'],
            btn: ['提交'],
            content: '/admin/authority/curd?curd=edit',
            yes: function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var formData = iframeWin.formData();
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: formData
                }, function (res) {
                    if (res.code === 0)
                        layer.close(index);
                });
            },
            success: function (layero, index) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                iframeWin.init(data);
            },
            end: function (layero, index) {
                Init();
            }
        });
    }

    function remove(data) {
        pearOper.confirm({
            title: "提示",
            message: "确定要删除该权限吗",
            success: function () {
                request.post({
                    url: MODULE_PATH + '/addOrUpdate',
                    data: {
                        id: data.id,
                        state: '删除'
                    }
                }, function (data) {
                    Init();
                });
            }
        });
    }

});