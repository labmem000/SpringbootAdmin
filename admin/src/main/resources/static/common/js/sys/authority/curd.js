var curdObj, tool, $;

function init(data) {
    curdObj = data;
    console.log(data)
    console.log(curd)
    layui.use(['form', 'jquery', 'request', 'tool'], function () {
        let table = layui.table;
        let form = layui.form;
        $ = layui.jquery;
        let request = layui.request;
        tool = layui.tool;
        form.render();
        if (curd === 'edit') {
            tool.JsonToHtmlValByName(data, $("#curdForm"))
            $("textarea[name='remarks']").text(data.remarks);
        }
    });
}

/**
 * 获取表单内容
 */
function formData() {
    var data = tool.serializeObject($("#curdForm"));
    if (curd === 'edit')
        data.id = curdObj.id;
    return data;
}