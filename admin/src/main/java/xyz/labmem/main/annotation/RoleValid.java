package xyz.labmem.main.annotation;

import java.lang.annotation.*;

/**
 * 权限验证注解
 * Created in 11:19 2020-3-9
 *
 * @author Lty
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RoleValid {
    boolean open() default true;

    String comment() default "未命名";

}
