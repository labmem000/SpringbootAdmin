package xyz.labmem.main.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 数据状态枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum StateEnum implements IEnum<String> {
    /**
     * 数据当前状态
     */
    OFF("OFF", "停用"),
    ON("ON", "启用"),
    ALL("ALL", "全部"),
    DEL("DEL", "删除");

    private String value;
    private String desc;

    @JsonCreator
    StateEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @JsonValue
    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

