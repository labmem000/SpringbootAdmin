package xyz.labmem.main.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 登陆类型枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LoginTypeEnum implements IEnum<String> {

    /**
     * 登陆类型枚举
     */
    WEB("WEB", "Web端"),
    APP("APP", "App端");

    private String value;
    private String desc;

    @JsonCreator
    LoginTypeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }


    @Override
    public String getValue() {
        return this.value;
    }


    public void setValue(String value) {
        this.value = value;
    }


    @JsonValue
    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
