package xyz.labmem.main.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

/**
 * Created by Lty
 * Created in 11:10 2020-3-5
 */
@Data
@ApiModel("统一返回json格式")
public class ResultVo<T> implements Serializable {

    @ApiModelProperty("状态码 0:处理成功；-1:处理失败；-2:表单验证失败；-3:登录失效")
    protected Integer code;

    @ApiModelProperty("提示信息")
    protected String message;

    @ApiModelProperty("返回时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime timestamp = now();

    @ApiModelProperty("返回数据")
    protected T data;

    public ResultVo() {
    }

    public ResultVo(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
