package xyz.labmem.main.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import static java.time.LocalDateTime.now;

/**
 * Created by Lty
 * Created in 11:10 2020-3-5
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("layui列表返回json格式")
public class ResultPageVo<T> extends ResultVo<T> implements Serializable {

    @ApiModelProperty("数据总数量")
    private long count;

    public ResultPageVo(Integer code, String message, T data, long count) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.count = count;
    }

}
