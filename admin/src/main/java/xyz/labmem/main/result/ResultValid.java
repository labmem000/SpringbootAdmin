package xyz.labmem.main.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Lty
 * Created in 11:50 2020-3-5
 */
@Data
@ApiModel("表单验证返回信息")
public class ResultValid implements Serializable {
    @ApiModelProperty("字段")
    private String field;

    @ApiModelProperty("提示")
    private String message;

    public ResultValid(String field, String message) {
        this.field = field;
        this.message = message;
    }
}
