package xyz.labmem.main.result;

import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 返回响应工具
 * Created by Lty
 * Created in 11:15 2020-3-5
 */
public class ResultUtil {

    public static final Integer SUCCESS_CODE = 0;
    public static final Integer FAILED_CODE = -1;
    public static final Integer FORM_FAILED_CODE = -2;
    public static final Integer LOGIN_FAILED_CODE = -3;
    public static final String SUCCESS_Message = "请求成功";
    public static final String FAILED_Message = "服务器错误";

    public static ResultVo free(Integer code, String message, Object data) {
        return new ResultVo<Object>(code, message, data);
    }

    public static ResultVo success(Object data) {
        return free(SUCCESS_CODE, SUCCESS_Message, data);
    }

    public static ResultVo success(Object data, String message) {
        return free(SUCCESS_CODE, message, data);
    }

    public static ResultVo successMessage(String message) {
        return success(null, message);
    }

    public static ResultVo success() {
        return success(null);
    }

    public static ResultVo failed(String message, Object data) {
        return free(FAILED_CODE, message, data);
    }

    public static ResultVo failedMessage(String message) {
        return failed(message, null);
    }

    public static ResultVo userFailed(String message) {
        return free(LOGIN_FAILED_CODE, message, null);
    }

    public static ResultVo failed(Object data) {
        return failed(FAILED_Message, data);
    }

    public static ResultVo failed() {
        return failed(null);
    }

    //表单验证失败
    public static ResultVo formFailed(String message, Object data) {
        return free(FORM_FAILED_CODE, message, data);
    }

    public static ResultVo formFailed() {
        return free(FORM_FAILED_CODE, "", null);
    }

    public static ResultPageVo page(IPage page) {
        return new ResultPageVo<Object>(0, SUCCESS_Message, page.getRecords(), page.getTotal());
    }

    public static ResultPageVo page(Object data, long count) {
        return new ResultPageVo<Object>(0, SUCCESS_Message, data, count);
    }


}
