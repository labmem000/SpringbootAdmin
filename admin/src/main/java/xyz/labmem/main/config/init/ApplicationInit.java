package xyz.labmem.main.config.init;

import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Repository;
import xyz.labmem.main.safety.RoleControllerInit;
import xyz.labmem.main.safety.RoleInit;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.Console;


/**
 * 应用启动之后立即检查执行
 */
@Repository
@Slf4j
public class ApplicationInit extends StaticVariable implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        new RoleInit();
        new RoleControllerInit();
    }
}
