package xyz.labmem.main.config.init;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import xyz.labmem.main.application.entity.sys.RoleValid;
import xyz.labmem.main.enums.StateEnum;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.AopTargetUtils;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by Lty
 * Created in 16:11 2020-3-9
 */
@Slf4j
public class ApplicationStartListener extends StaticVariable implements ApplicationListener<ContextRefreshedEvent> {
    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("======初始化接口权限======");
        //@Controller包含@RestController
        Map<String, Object> ControllerBeans = contextRefreshedEvent.getApplicationContext().getBeansWithAnnotation(Controller.class);
        putApiRoleValid(ControllerBeans);
    }

    public void putApiRoleValid(Map<String, Object> Beans) throws Exception {
        RoleValid roleValidEntity = new RoleValid();
        for (String key : Beans.keySet()) {
            //接口类名
            Class clz = AopTargetUtils.getTarget(Beans.get(key)).getClass();
            String ClassName = clz.getName();
            for (Method method : clz.getMethods()) {
                //权限注解对应
                xyz.labmem.main.annotation.RoleValid roleValid = method.getAnnotation(xyz.labmem.main.annotation.RoleValid.class);
                if (roleValid != null) {
                    //接口方法名
                    String methodName = method.getName();
                    //该接口是否启用权限
                    boolean open = roleValid.open();
                    String apiOperationVar = roleValid.comment();
                    if (open) {
                        //获取当前接口的注释
                        ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
                        if (apiOperation != null) {
                            apiOperationVar = apiOperation.value();
                        }
                        RoleValid roleEntity = roleValidEntity.selectOne(
                                new LambdaQueryWrapper<RoleValid>()
                                        .eq(RoleValid::getClassName, ClassName)
                                        .eq(RoleValid::getMethodName, methodName)
                                        .ne(RoleValid::getState, StateEnum.DEL)
                        );
                        //获取对象是否是APIController
                        boolean isAPI = method.getDeclaringClass().isAnnotationPresent(RestController.class);
                        //创建新接口权限信息
                        RoleValid newEntity = new RoleValid(apiOperationVar, ClassName, methodName, isAPI);
                        if (roleEntity == null) {
                            NewRoleValidEntities.add(newEntity);
                        } else {
                            //获取之前所有有效权限接口
                            if (roleEntity.getState() == StateEnum.ON) {
                                RoleValidEntities.put(ClassName + "." + methodName, roleEntity.getRoleList());
                            }
                        }
                    }
                }
            }
        }
    }

}
