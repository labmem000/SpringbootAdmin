package xyz.labmem.main.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import static java.time.LocalDateTime.now;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/19 16:11
 * @description：MybatisPlus插入和更新切面
 * @modified By：
 * @version: $
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createTime", now(), metaObject);
        this.setFieldValByName("updateTime", now(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", now(), metaObject);
    }
}
