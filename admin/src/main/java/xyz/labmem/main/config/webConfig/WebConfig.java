package xyz.labmem.main.config.webConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.labmem.main.application.service.sys.impl.FileServiceImpl;

import java.io.File;


/**
 * @author ：刘天予
 * @date ：Created in 2020/4/23 17:29
 */
@Configuration
@Component
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Value("${filePath}")
    private String fileDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //swaggerUI
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        //静态文件
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/static/favicon.ico");
        //获取文件
        registry.addResourceHandler("/getFile/**").addResourceLocations("file:" + fileDir);
    }
}
