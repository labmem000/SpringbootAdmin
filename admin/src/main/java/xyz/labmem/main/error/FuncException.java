package xyz.labmem.main.error;

import cn.hutool.core.exceptions.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Lty
 * Created in 13:51 2020-3-5
 */
@Slf4j
public class FuncException extends RuntimeException {

    public FuncException(String message, Exception e) {
        log.error("===============报错信息 (FuncException)===============");
        log.error("message：" + message);
        log.error(ExceptionUtil.getMessage(e));
        log.error("=====================ERROR  END=======================");
    }

}
