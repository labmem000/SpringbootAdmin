package xyz.labmem.main.constants;

/**
 * 通用的常量参数
 */
public interface ConstantsRole {

    /**
     * Token requestHeader的key
     */
    String REQUEST_HEADER_TOKEN_KEY = "labmem_token";


    Integer TOKEN_VALID_DATE_HOUR = 12;

}
