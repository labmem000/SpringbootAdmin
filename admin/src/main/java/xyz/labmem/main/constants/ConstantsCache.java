package xyz.labmem.main.constants;

/**
 * 缓存常量
 */
public interface ConstantsCache {

    /**
     * Redis缓存测试value
     */
    String REDIS_DEFAULT_CACHE_NAME = "TEST";

    /**
     * Redis缓存默认key的配置
     */
    String REDIS_DEFAULT_CACHE_KEY = "#root.targetClass.getName() + #root.methodName";


    /**
     * Redis自定义存储默认缓存时间
     */
    int REDIS_DEFAULT_CUSTOM_TIMEOUT = 5;

}
