package xyz.labmem.main.constants;

/**
 * Redis常量
 */
public interface ConstantsRedis {

    /**
     * Token的Redis前缀
     */
    String TOKEN_PREFIX = "TOKEN==";

    /**
     * 用户邮箱验证前缀
     */
    String USER_EMAIl_VAILD_PREFIX = "USER-EMAIl-VAILD==";

}
