package xyz.labmem.main.constants;

/**
 * 通用的常量参数
 */
public interface ConstantsRegular {

    /**
     * ORM中最后一句为限制最多输出一个
     */
    String SQL_LAST_LIMIT_ONE = "LIMIT 1";

    /**
     * 日期格式化Formatter
     */
    String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 时间日期格式化Formatter
     */
    String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 时间格式化Formatter
     */
    String TIME_FORMAT = "HH:mm:ss";

    /**
     * Hutool工具中的http超时时间，单位是毫秒
     */
    Integer HUTOOL_HTTP_TIMEOUT = 30 * 1000;
}
