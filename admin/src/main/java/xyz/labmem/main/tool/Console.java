package xyz.labmem.main.tool;

import cn.hutool.core.convert.Convert;
import cn.hutool.setting.Setting;

/**
 * 重載hutool.Console的方法，加入一个开关
 * Created by Lty
 * Created in 11:52 2020-3-3
 */
public class Console extends cn.hutool.core.lang.Console {

    private static boolean debug = true;

    public static void log(Object obj) {
        if (debug)
            cn.hutool.core.lang.Console.log(obj);
    }

    public static void print(Object obj) {
        if (debug)
            cn.hutool.core.lang.Console.print(obj);
    }

    public static void printProgress(char showChar, int len) {
        if (debug)
            cn.hutool.core.lang.Console.printProgress(showChar, len);
    }

    public static void printProgress(char showChar, int totalLen, double rate) {
        if (debug)
            cn.hutool.core.lang.Console.printProgress(showChar, totalLen, rate);
    }

    public static void log(String template, Object... values) {
        if (debug)
            cn.hutool.core.lang.Console.log(template, values);
    }

    public static void print(String template, Object... values) {
        if (debug)
            cn.hutool.core.lang.Console.print(template, values);
    }

    public static void log(Throwable t, String template, Object... values) {
        if (debug)
            cn.hutool.core.lang.Console.log(t, template, values);
    }


}
