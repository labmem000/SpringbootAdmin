package xyz.labmem.main.tool;

import org.springframework.stereotype.Repository;
import xyz.labmem.main.constants.ConstantsRole;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Lty
 * Created in 23:21 2020/3/10
 */
@Repository
public class CookieUtils {

    public static String getCookie(HttpServletRequest request, String cookieName) {

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }

    public void writeCookie(HttpServletResponse response, String cookieName, String value) {
        Cookie cookie = new Cookie(cookieName, value);
        cookie.setPath("/");
        cookie.setMaxAge(3600 * ConstantsRole.TOKEN_VALID_DATE_HOUR);
        response.addCookie(cookie);
    }

}
