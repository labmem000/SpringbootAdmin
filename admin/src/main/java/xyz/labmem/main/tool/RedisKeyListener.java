package xyz.labmem.main.tool;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * redis监听key操作
 * Created by Lty
 * Created in 11:59 2020-3-5
 */
@Component
public class RedisKeyListener extends KeyExpirationEventMessageListener {

    public RedisKeyListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /**
     * redis key销毁时回调，进行数据处理
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 用户做自己的业务处理即可,注意message.toString()可以获取失效的key
        String expiredKey = message.toString();
        //TODO
        Console.log("key:" + expiredKey + "销毁！");
    }
}
