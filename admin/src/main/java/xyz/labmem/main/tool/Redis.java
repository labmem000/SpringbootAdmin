package xyz.labmem.main.tool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import xyz.labmem.main.constants.ConstantsRedis;

import java.util.concurrent.TimeUnit;

/**
 * Created by 刘天予
 * 2018/4/30 16:11
 */
@Repository
public class Redis implements ConstantsRedis {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    public void add(String key, String value, Integer time, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key, value, time, timeUnit);
    }

    public String get(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    public void delete(String key) {
        stringRedisTemplate.opsForValue().getOperations().delete(key);
    }

    public boolean isEmpty(String key) {
        if (key == null || key.equals(""))
            return true;
        return stringRedisTemplate.opsForValue().get(key) == null;
    }
}
