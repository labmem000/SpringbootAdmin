package xyz.labmem.main.safety;

import lombok.extern.slf4j.Slf4j;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.Jackson;

import java.util.ArrayList;
import java.util.List;

/**
 * 接口权限初始化
 * Created by Lty
 * Created in 10:50 2020-3-6
 */
@Slf4j
public class RoleControllerInit extends StaticVariable {

    /**
     * 接口权限初始化
     */
    public RoleControllerInit() {
        List<String> rolesDefList = new ArrayList<>();
        rolesDefList.add(rolesDef.getRoleEnum());
        String RoleList = Jackson.ObjectToJson(rolesDefList);
        //新接口权限存入数据库
        NewRoleValidEntities.parallelStream()
                .forEach(r -> {
                    //放入默认权限
                    r.setRoleList(RoleList);
                    r.insert();
                    RoleValidEntities.put(r.getClassName() + "." + r.getMethodName(), RoleList);
                });
        //释放静态资源
        NewRoleValidEntities = null;
        log.info("======初始化接口权限 结束======");
    }

}
