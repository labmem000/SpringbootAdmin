package xyz.labmem.main.safety;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import xyz.labmem.main.application.entity.sys.Role;
import xyz.labmem.main.enums.StateEnum;
import xyz.labmem.main.sys.StaticVariable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 角色权限初始化
 * Created by Lty
 * Created in 10:50 2020-3-6
 */
public class RoleInit extends StaticVariable {

    /**
     * 角色权限初始化
     */
    public RoleInit() {
        Role roleEntity = new Role();
        List<Role> roleEntityList = roleEntity.selectList(
                new LambdaQueryWrapper<Role>()
                        .ne(Role::getState, StateEnum.DEL)
        );
        Map<String, Role> roleMap = new HashMap<>();
        AtomicReference<Role> rolesD = new AtomicReference<>(new Role());
        roleEntityList.forEach(r -> {
            roleMap.put(r.getRoleEnum(), r);
            if ("1".equals(r.getRoleDefault())) {
                rolesD.set(r);
            }
        });
        roles = roleMap;
        rolesDef = rolesD.get();
    }

}
