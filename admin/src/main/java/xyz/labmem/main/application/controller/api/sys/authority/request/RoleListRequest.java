package xyz.labmem.main.application.controller.api.sys.authority.request;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import org.springframework.util.StringUtils;
import xyz.labmem.main.application.entity.sys.Role;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/8 17:48
 * @description：
 * @modified By：
 * @version: $
 */
@Data
public class RoleListRequest extends Role {

    private String page;

    private String limit;

    public Page<Role> getPage() {
        Page<Role> RequestPage = new Page<>();
        if (!StringUtils.isEmpty(page)) {
            RequestPage.setCurrent(Long.parseLong(page));
        }
        if (!StringUtils.isEmpty(limit)) {
            RequestPage.setSize(Long.parseLong(limit));
        }
        return RequestPage;
    }
}
