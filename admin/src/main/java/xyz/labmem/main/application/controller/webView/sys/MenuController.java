package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.labmem.main.annotation.RoleValid;


/**
 * @author ：刘天予
 * @date ：Created in 2020/5/8 11:07
 * @description：菜单
 */
@Controller
@RequestMapping("/admin/menu")
public class MenuController {

    @GetMapping("/index")
    @RoleValid(comment = "菜单管理页面")
    public String index() {
        return "sys/menu/index";
    }

    @GetMapping("/curd")
    @RoleValid(comment = "菜单管理curd页面")
    public String curd(@RequestParam String curd, Model model) {
        model.addAttribute("curd", curd);
        return "sys/menu/curd";
    }

    @GetMapping("/pickList")
    @RoleValid(comment = "选择菜单")
    public String pickList() {
        return "sys/menu/pickList";
    }

}
