package xyz.labmem.main.application.mapper.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import xyz.labmem.main.application.entity.sys.User;

/**
 * Created by Lty
 * Created in 10:45 2020-3-6
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    @Select("select u.id,u.user_name,u.email,u.nick_name,r.role_name as roleName,u.is_admin,u.state,u.role " +
            "from sys_user u " +
            "left join sys_role r on u.role=r.role_enum " +
            "${ew.customSqlSegment}")
    IPage<User> selectUserPage(IPage<User> page, @Param(Constants.WRAPPER) QueryWrapper<Object> queryWrapper);

}
