package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.labmem.main.annotation.RoleValid;
import xyz.labmem.main.application.entity.sys.User;
import xyz.labmem.main.application.service.sys.impl.FileServiceImpl;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.ImgUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lty
 * Created in 21:51 2020/3/10
 */
@Controller
@RequestMapping("/admin/user")
public class UserController extends StaticVariable {

    private final FileServiceImpl fileService;

    public UserController(FileServiceImpl fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/index")
    @RoleValid(comment = "用户管理")
    public String index() {
        return "sys/user/index";
    }

    @GetMapping("/curd")
    @RoleValid(comment = "用户管理curd页面")
    public String curd(@RequestParam String curd, Model model) {
        model.addAttribute("curd", curd);
        return "sys/user/curd";
    }

    @GetMapping("/person")
    @RoleValid(comment = "个人信息")
    public String person(Model model) {
        User user = this.getUser().selectById();
        Map<String, String> imgJson = new HashMap<>();
        File headImg = fileService.getFileByKey(user.getHeadImg());
        if (headImg != null) {
            imgJson.put(user.getHeadImg(), ImgUtil.imageToBase64Str(headImg));
        }
        model.addAttribute("headImg", imgJson);
        model.addAttribute("user", this.getUser());
        return "sys/user/person";
    }

}
