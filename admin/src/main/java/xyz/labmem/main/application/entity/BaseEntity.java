package xyz.labmem.main.application.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import xyz.labmem.main.enums.StateEnum;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/19 16:34
 */
@Data
public abstract class BaseEntity<T extends Model<?>> extends Model<T> implements Serializable {

    @TableId
    @ApiModelProperty(value = "数据主键")
    private String id;

    @ApiModelProperty(value = "数据创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "数据修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "当前数据状态")
    private StateEnum state = StateEnum.ON;

}
