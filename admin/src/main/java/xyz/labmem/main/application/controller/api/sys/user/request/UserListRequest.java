package xyz.labmem.main.application.controller.api.sys.user.request;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.StringUtils;
import xyz.labmem.main.application.entity.sys.User;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/15 15:16
 * @description：
 * @modified By：
 * @version: $
 */
public class UserListRequest extends User {

    private String page;

    private String limit;

    public Page<User> getPage() {
        Page<User> RequestPage = new Page<>();
        if (!StringUtils.isEmpty(page)) {
            RequestPage.setCurrent(Long.parseLong(page));
        }
        if (!StringUtils.isEmpty(limit)) {
            RequestPage.setSize(Long.parseLong(limit));
        }
        return RequestPage;
    }

}
