package xyz.labmem.main.application.controller.api.sys.user.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/17 14:04
 * @description：
 * @modified By：
 * @version: $
 */
@Data
public class UpdatePasswordRequest {

    @ApiModelProperty("原密码")
    @NotNull(message = "必填项")
    private String password;

    @ApiModelProperty("新密码")
    @NotNull(message = "必填项")
    @Size(min = 6, max = 16)
    private String newPassword;

}
