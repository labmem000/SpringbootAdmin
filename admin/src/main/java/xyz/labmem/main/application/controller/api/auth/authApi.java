package xyz.labmem.main.application.controller.api.auth;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.labmem.main.annotation.RoleValid;
import xyz.labmem.main.application.controller.api.auth.request.LoginRequestBody;
import xyz.labmem.main.application.controller.api.auth.request.RegisterRequestBody;
import xyz.labmem.main.application.entity.sys.User;
import xyz.labmem.main.application.service.user.impl.UserServiceImpl;
import xyz.labmem.main.constants.ConstantsRedis;
import xyz.labmem.main.result.ResultUtil;
import xyz.labmem.main.result.ResultVo;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.Redis;

import javax.validation.Valid;

/**
 * Created by Lty
 * Created in 11:00 2020-3-6
 */
@RestController
@Api(tags = "用户权限API")
@RequestMapping("/auth")
public class authApi extends StaticVariable {

    private final UserServiceImpl userService;

    private final Redis redis;

    public authApi(UserServiceImpl userService, Redis redis) {
        this.userService = userService;
        this.redis = redis;
    }

    @ApiOperation("用户登录")
    @PostMapping("/login")
    public ResultVo login(@RequestBody @Valid LoginRequestBody loginRequestBody) {
        if (!userService.isOk(loginRequestBody))
            return ResultUtil.failedMessage("账号失效");
        return loginToken(loginRequestBody);
    }

    @ApiOperation("后台管理登录")
    @PostMapping("/adminLogin")
    public ResultVo adminLogin(@RequestBody @Valid LoginRequestBody loginRequestBody) {
        if (!userService.adminIsOk(loginRequestBody))
            return ResultUtil.failedMessage("账号禁用");
        return loginToken(loginRequestBody);
    }

    @ApiOperation("用户注销")
    @PostMapping("/logout")
    @RoleValid
    public ResultVo logout() {
        User user = this.getUser();
        redis.delete(ConstantsRedis.TOKEN_PREFIX + user.getToken());
        return ResultUtil.success();
    }

    @ApiOperation("用户注册")
    @PostMapping("/register")
    public ResultVo register(@RequestBody @Valid RegisterRequestBody registerRequestBody) {
        if (!userService.registerValidEmail(registerRequestBody)) {
            return ResultUtil.failedMessage("邮箱已被使用");
        }
        if (!userService.registerValidUserName(registerRequestBody)) {
            return ResultUtil.failedMessage("账号已存在");
        }
        if (!userService.registerValidNickName(registerRequestBody)) {
            return ResultUtil.failedMessage("昵称已被使用");
        }
        if (userService.register(registerRequestBody)) {
            return ResultUtil.successMessage("注册成功，请前往邮箱验证");
        }
        return ResultUtil.failedMessage("注册失败");
    }

    @ApiOperation("用户信息")
    @PostMapping("/userInfo")
    @RoleValid
    public ResultVo userInfo() {
        return ResultUtil.success(this.getUser());
    }


    /**
     * 登录获取token
     *
     * @param loginRequestBody
     * @return
     */
    private ResultVo loginToken(LoginRequestBody loginRequestBody) {
        String token = userService.login(loginRequestBody);
        if (StrUtil.isNotEmpty(token)) {
            return ResultUtil.success(token);
        } else if ("".equals(token)) {
            return ResultUtil.failedMessage("账号未通过邮箱验证");
        } else {
            return ResultUtil.failedMessage("用户名密码错误");
        }
    }

}
