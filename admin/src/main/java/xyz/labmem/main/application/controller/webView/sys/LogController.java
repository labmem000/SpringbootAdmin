package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.labmem.main.annotation.RoleValid;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/24 11:14
 * @description：
 * @modified By：
 * @version: $
 */
@Controller
@RequestMapping("/admin/log")
public class LogController {

    @GetMapping("/login")
    @RoleValid(comment = "登录日志页面")
    public String index() {
        return "sys/logs/loginLogs";
    }
}
