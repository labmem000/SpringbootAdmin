package xyz.labmem.main.application.entity.sys;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xyz.labmem.main.application.controller.api.auth.request.RegisterRequestBody;
import xyz.labmem.main.application.entity.BaseEntity;

/**
 * Created by Lty
 * Created in 9:55 2020-3-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "用户表")
public class User extends BaseEntity<User> {

    @ApiModelProperty(value = "账号")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String headImg;

    @ApiModelProperty(value = "权限")
    private String role;

    @ApiModelProperty(value = "权限名称")
    @TableField(exist = false)
    private String roleName;

    @ApiModelProperty(value = "是否是后台管理账号")
    private String isAdmin;

    @TableField(exist = false)
    private String token;

    public User() {
        super();
    }

    public User(RegisterRequestBody newUser) {
        this.userName = newUser.getUserName();
        this.password = SecureUtil.sha1(newUser.getPassword());
        this.email = newUser.getEmail();
        this.nickName = newUser.getNickName();
        this.role = "USER";
    }

    public void resetPwd() {
        this.password = SecureUtil.sha1("labmem123123");
    }

}
