package xyz.labmem.main.application.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import xyz.labmem.main.application.entity.sys.Menu;

/**
 * Created by Lty
 * Created in 10:45 2020-3-6
 */
@Repository
public interface MenuMapper extends BaseMapper<Menu> {
}
