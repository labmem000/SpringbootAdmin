package xyz.labmem.main.application.controller.api.sys.log.request;

import lombok.Data;
import xyz.labmem.main.application.entity.sys.LoginLogs;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/24 11:34
 * @description：
 * @modified By：
 * @version: $
 */
@Data
public class LoginLogRequest extends LoginLogs {

    private Integer page;

    private Integer limit;

}
