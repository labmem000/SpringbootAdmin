package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.labmem.main.annotation.RoleValid;


/**
 * @author ：刘天予
 * @date ：Created in 2020/5/8 11:07
 * @description：接口权限管理
 */
@Controller
@RequestMapping("/admin/authorityApi")
public class AuthorityApiController {

    @GetMapping("/index")
    @RoleValid(comment = "权限管理页面")
    public String index() {
        return "sys/authority/apiIndex";
    }

    @GetMapping("/validRole")
    @RoleValid(comment = "权限管理角色分配页面")
    public String validRole() {
        return "sys/authority/apiValidRole";
    }

}
