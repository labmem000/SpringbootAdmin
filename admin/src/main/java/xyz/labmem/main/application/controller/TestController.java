package xyz.labmem.main.application.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import xyz.labmem.main.config.jackson.JacksonObjectMapper;
import xyz.labmem.main.application.entity.sys.User;
import xyz.labmem.main.tool.Console;
import xyz.labmem.main.tool.Jackson;
import xyz.labmem.main.tool.Redis;

/**
 * Created by Lty
 * Created in 15:56 2020-3-6
 */
@RestController
public class TestController {

    @Autowired
    private Redis redis;

    @GetMapping("/test")
    public String test() throws JsonProcessingException {
        User user=new User();
        user.setId("222");
        user.setEmail("ttttt");
       return new JacksonObjectMapper()
                .serializingObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(user);
//        return NetUtil.getServerAddr();
    }

    @GetMapping("/testToken/{token}")
    public String testToken(@PathVariable String token) {
        String userString = redis.get(redis.TOKEN_PREFIX + token);
        User user=Jackson.JsonToObject(userString,User.class);
        Console.log(user);
        return user.toString();
    }

}
