package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.labmem.main.annotation.RoleValid;


/**
 * @author ：刘天予
 * @date ：Created in 2020/5/8 11:07
 * @description：权限管理
 */
@Controller
@RequestMapping("/admin/authority")
public class AuthorityController {

    @GetMapping("/index")
    @RoleValid(comment = "权限角色页面")
    public String index() {
        return "sys/authority/index";
    }

    @GetMapping("/curd")
    @RoleValid(comment = "权限管理curd页面")
    public String curd(@RequestParam String curd, Model model) {
        model.addAttribute("curd", curd);
        return "sys/authority/curd";
    }

    @GetMapping("/pickList")
    @RoleValid(comment = "选择权限列表")
    public String pickList() {
        return "sys/authority/pickList";
    }


}
