package xyz.labmem.main.application.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xyz.labmem.main.application.entity.BaseEntity;


/**
 * Created by Lty
 * Created in 9:55 2020-3-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "菜单表")
public class Menu extends BaseEntity<Menu> {

    @ApiModelProperty(value = "父id")
    private String pid;

    @ApiModelProperty(value = "菜单标题")
    private String title;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "菜单类型 0、父菜单1、菜单按钮")
    private Integer type;

    @ApiModelProperty(value = "菜单地址")
    private String href;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "窗口类型")
    private String openType;

    @ApiModelProperty(value = "权限")
    private String roleList;

}
