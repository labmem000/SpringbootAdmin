package xyz.labmem.main.application.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xyz.labmem.main.application.entity.BaseEntity;
import xyz.labmem.main.tool.Jackson;

import java.util.List;


/**
 * Created by Lty
 * Created in 9:55 2020-3-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role_valid")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "接口权限列表")
public class RoleValid extends BaseEntity<RoleValid> {

    @ApiModelProperty(value = "接口注释")
    private String comment;

    @ApiModelProperty(value = "接口类名")
    private String className;

    @ApiModelProperty(value = "接口方法名")
    private String methodName;

    @ApiModelProperty(value = "接口对应的权限")
    private String roleList;

    @ApiModelProperty(value = "接口对应的权限名称")
    @TableField(exist = false)
    private String roleName;

    @ApiModelProperty(value = "是否是api接口")
    private Integer isApi;

    public RoleValid() {
        super();
    }

    public RoleValid(String comment, String className, String methodName, boolean isApi) {
        this.comment = comment;
        this.className = className;
        this.methodName = methodName;
        this.roleList = "NULL";
        this.isApi = isApi ? 1 : 0;
    }


    public RoleValid(String comment, String className, String methodName, List<String> roleList) {
        this.comment = comment;
        this.className = className;
        this.methodName = methodName;
        this.roleList = Jackson.ObjectToJson(roleList);
    }

}
