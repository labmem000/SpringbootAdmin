package xyz.labmem.main.application.controller.api.sys.authority.request;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;
import xyz.labmem.main.application.entity.sys.Role;
import xyz.labmem.main.application.entity.sys.RoleValid;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/8 17:48
 * @description：
 * @modified By：
 * @version: $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleValidListRequest extends RoleValid {

    private String page;

    private String limit;

    public Page<RoleValid> getPage() {
        Page<RoleValid> RequestPage = new Page<>();
        if (!StringUtils.isEmpty(page)) {
            RequestPage.setCurrent(Long.parseLong(page));
        }
        if (!StringUtils.isEmpty(limit)) {
            RequestPage.setSize(Long.parseLong(limit));
        }
        return RequestPage;
    }
}
