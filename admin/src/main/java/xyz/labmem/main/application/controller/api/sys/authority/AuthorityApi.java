package xyz.labmem.main.application.controller.api.sys.authority;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import xyz.labmem.main.annotation.RoleValid;
import xyz.labmem.main.application.controller.api.sys.authority.request.RoleListRequest;
import xyz.labmem.main.application.controller.api.sys.menu.response.MenuListResponse;
import xyz.labmem.main.application.controller.api.sys.menu.response.MenuTreeResponse;
import xyz.labmem.main.application.entity.sys.Menu;
import xyz.labmem.main.application.entity.sys.Role;
import xyz.labmem.main.enums.StateEnum;
import xyz.labmem.main.result.ResultPageVo;
import xyz.labmem.main.result.ResultUtil;
import xyz.labmem.main.result.ResultVo;
import xyz.labmem.main.sys.StaticVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/8 11:07
 * @description：权限
 */
@RestController
@Api(tags = "后台菜单API")
@RequestMapping("/admin/authority/api")
public class AuthorityApi extends StaticVariable {

    /**
     * @return
     */
    @PostMapping("/pageList")
    @RoleValid
    @ApiOperation("权限分页列表")
    public ResultVo authorityPageList(@RequestBody RoleListRequest request) {

        LambdaQueryWrapper<Role> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        String roleName = request.getRoleName();
        if (StrUtil.isNotEmpty(roleName)) {
            lambdaQueryWrapper.like(Role::getRoleName, roleName);
        }
        String roleEnum = request.getRoleEnum();
        if (StrUtil.isNotEmpty(roleEnum)) {
            lambdaQueryWrapper.like(Role::getRoleEnum, roleEnum);
        }
        if (request.getState() != StateEnum.ALL) {
            lambdaQueryWrapper.eq(Role::getState, request.getState().getValue());
        } else {
            lambdaQueryWrapper.ne(Role::getState, StateEnum.DEL);
        }
        IPage<Role> RolePage = new Role().selectPage(request.getPage(), lambdaQueryWrapper);
        return ResultUtil.page(RolePage);
    }

    /**
     * @return
     */
    @PostMapping("/list")
    @RoleValid
    @ApiOperation("权限列表")
    public ResultVo authorityList() {
        List<Role> roles = new Role().selectList(new LambdaQueryWrapper<Role>().eq(Role::getState, StateEnum.ON));
        return ResultUtil.success(roles);
    }

    /**
     * @return
     */
    @PostMapping("/addOrUpdate")
    @RoleValid
    @ApiOperation("增加或修改权限")
    public ResultVo authorityAddOrUpdate(@RequestBody Role role) {
        if (role.getRoleDefault() != null && role.getRoleDefault().equals("1")) {
            Role RoleDef = new Role();
            RoleDef.setRoleDefault("0");
            RoleDef.update(new LambdaQueryWrapper<Role>().eq(Role::getRoleDefault, "1"));
        }
        if (role.insertOrUpdate()) {
            Role RO = role.selectById();
            roles.put(RO.getRoleEnum(), RO);
            return ResultUtil.success();
        } else
            return ResultUtil.failed();
    }


}
