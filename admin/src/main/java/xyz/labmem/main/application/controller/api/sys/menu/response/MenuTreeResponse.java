package xyz.labmem.main.application.controller.api.sys.menu.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import xyz.labmem.main.application.entity.sys.Menu;

import java.util.List;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/8 11:35
 * @description：菜单树
 * @modified By：
 * @version: $
 */
@Data
public class MenuTreeResponse {

    @ApiModelProperty(value = "菜单标题")
    private String title;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "菜单类型 0、父菜单1、菜单按钮")
    private Integer type;

    @ApiModelProperty(value = "菜单地址")
    private String href;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "窗口类型")
    private String openType;

    @ApiModelProperty(value = "数据主键")
    private String id;

    @ApiModelProperty(value = "子菜单")
    private List<MenuTreeResponse> children;

    public MenuTreeResponse() {
    }

    public MenuTreeResponse(Menu menu) {
        this.id = menu.getId();
        this.title = menu.getTitle();
        this.icon = menu.getIcon();
        this.type = menu.getType();
        this.href = menu.getHref();
        this.sort = menu.getSort();
        this.openType = menu.getOpenType();
    }

}
