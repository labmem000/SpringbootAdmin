package xyz.labmem.main.application.entity.sys;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xyz.labmem.main.application.controller.api.auth.request.RegisterRequestBody;
import xyz.labmem.main.application.entity.BaseEntity;

import java.text.DecimalFormat;

/**
 * Created by Lty
 * Created in 9:55 2020-3-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_file")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "文件表")
public class File extends BaseEntity<File> {

    @ApiModelProperty(value = "文件原名")
    private String originalName;

    @ApiModelProperty(value = "文件保存路径")
    private String filePath;

    @ApiModelProperty(value = "文件访问Key")
    private String fileKey;

    @ApiModelProperty(value = "文件大小")
    private String fileSize;

    public static String getSize(Long size) {
        long KB = 1024;//定义KB的计算常量
        long MB = KB * 1024;//定义MB的计算常量
        long GB = MB * 1024;//定义GB的计算常量
        long TB = GB * 1024;//定义GB的计算常量
        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
        String resultSize = "";
        if (size / TB >= 1) {
            resultSize = df.format(size / (float) TB) + "TB   ";
        } else if (size / GB >= 1) {
            //如果当前Byte的值大于等于1GB
            resultSize = df.format(size / (float) GB) + "GB   ";
        } else if (size / MB >= 1) {
            //如果当前Byte的值大于等于1MB
            resultSize = df.format(size / (float) MB) + "MB   ";
        } else if (size / KB >= 1) {
            //如果当前Byte的值大于等于1KB
            resultSize = df.format(size / (float) KB) + "KB   ";
        } else {
            resultSize = size + "B   ";
        }
        return resultSize;
    }

}
