package xyz.labmem.main.application.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xyz.labmem.main.application.entity.BaseEntity;

/**
 * Created by Lty
 * Created in 9:55 2020-3-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "角色权限列表")
public class Role extends BaseEntity<Role> {

    @ApiModelProperty(value = "权限名称")
    private String roleName;

    @ApiModelProperty(value = "权限枚举")
    private String roleEnum;

    @ApiModelProperty(value = "权限备注信息")
    private String remarks;

    @ApiModelProperty(value = "是否是默认权限")
    private String roleDefault;

    public Role() {
        super();
    }

}
