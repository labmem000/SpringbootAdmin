package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/19 11:01
 * @description：错误页面
 * @modified By：
 * @version: $
 */
@Controller
public class ErrorController {

    @GetMapping("403")
    public String notAuthority() {
        return "error/403";
    }

    @GetMapping("404")
    public String notFind() {
        return "error/404";
    }

    @GetMapping("500")
    public String serviceError() {
        return "error/500";
    }

    @GetMapping("503")
    public String Refuse() {
        return "error/503";
    }

}
