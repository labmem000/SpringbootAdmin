package xyz.labmem.main.application.controller.api.auth.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Lty
 * Created in 11:05 2020-3-6
 */
@Data
@ApiModel("用户注册表单")
public class RegisterRequestBody {

    @ApiModelProperty("用户名")
    @NotNull(message = "必填项")
    @Size(min = 4, max = 20)
    private String userName;

    @ApiModelProperty("密码")
    @NotNull(message = "必填项")
    @Size(min = 6, max = 16)
    private String password;

    @ApiModelProperty("邮箱")
    @NotNull(message = "必填项")
    @Email(regexp = "^(\\w-*\\.*)+@(\\w-?)+(\\.\\w{2,})+$")
    private String email;

    @ApiModelProperty("昵称")
    @NotNull(message = "必填项")
    @Size(min = 1, max = 12)
    private String nickName;

}
