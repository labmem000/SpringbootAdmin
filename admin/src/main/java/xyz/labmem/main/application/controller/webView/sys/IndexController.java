package xyz.labmem.main.application.controller.webView.sys;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import xyz.labmem.main.annotation.RoleValid;
import xyz.labmem.main.application.entity.sys.User;
import xyz.labmem.main.application.service.sys.impl.FileServiceImpl;
import xyz.labmem.main.sys.StaticVariable;
import xyz.labmem.main.tool.ImgUtil;

import java.io.File;

/**
 * @author ：刘天予
 * @date ：Created in 2020/4/23 17:41
 * @description：后台页面
 */
@Controller
public class IndexController extends StaticVariable {

    private final FileServiceImpl fileService;

    public IndexController(FileServiceImpl fileService) {
        this.fileService = fileService;
    }

    /**
     * 后台入口
     *
     * @return
     */
    @GetMapping("/")
    @RoleValid(comment = "平台入口")
    public String entrance(Model model) {
        User user = this.getUser();
        model.addAttribute("user", user);
        File headImg = fileService.getFileByKey(user.getHeadImg());
        if (headImg != null)
            model.addAttribute("headImg", ImgUtil.imageToBase64Str(headImg));
        else
            model.addAttribute("headImg", "/static/common/images/avatar.jpg");
        return "index";
    }

    /**
     * 首页
     *
     * @return
     */
    @GetMapping("/index")
    @RoleValid(comment = "首页")
    public String index() {
        return "sys/console";
    }

    /**
     * 后台登录页
     *
     * @return
     */
    @GetMapping("login")
    public String login() {
        return "login";
    }
}
