package xyz.labmem.main.application.entity.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/24 9:44
 * @description：登录日志
 * @modified By：
 * @version: $
 */
@Data
@ApiModel(value = "登录日志表")
public class LoginLogs {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "登录ip")
    private String ip;

    @ApiModelProperty(value = "浏览器信息")
    private String browser;

    @ApiModelProperty(value = "操作信息")
    private String message;

    @ApiModelProperty(value = "登录时间")
    private String loginDate;


}
