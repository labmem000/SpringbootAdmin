package xyz.labmem.main.application.controller.api.sys.authority.request;

import lombok.Data;

/**
 * @author ：刘天予
 * @date ：Created in 2020/6/8 17:48
 * @description：
 * @modified By：
 * @version: $
 */
@Data
public class RoleValidRequest {

    private String where;

    private String roleList;

    private Integer type;

    private String change;

}
