package xyz.labmem.main.application.controller.webView;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import xyz.labmem.main.application.service.user.impl.UserServiceImpl;

/**
 * Created by Lty
 * Created in 16:24 2020-3-6
 */
@Controller
public class ActiveController {

    private final UserServiceImpl userService;

    public ActiveController(UserServiceImpl userService) {
        this.userService = userService;
    }

    /**
     * 邮箱验证
     *
     * @param key
     * @return
     */
    @GetMapping("/active_email/{key}")
    public String activeEmail(@PathVariable String key) {
        if (userService.ValidEmail(key)) {
            return "valid/active";
        } else {
            return "error/404";
        }
    }

}
