# labmem后台管理框架

#### 介绍
本项目手动搭建权限验证系统及后台管理的快速开发框架（停止开发）

新项目基于spring security搭建演示地址：
http://aliyun.labmem.xyz:8000/admin
user:guest
passwd:123456

#### 软件架构
SpringBoot&
mybatis plus&
Pear-Admin-Layui&
swagger等

#### 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0619/101400_6d4aeab1_1661560.png "QQ截图20200619101250.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0619/101429_da45d1c3_1661560.png "QQ截图20200619101357.png")


#### 开源地址

Gitee     开源地址 : https://gitee.com/labmem000/labmemAdmin

### 开源大佬

 [Pear-Admin-Layui](https://gitee.com/Jmysy/Pear-Admin-Layui)